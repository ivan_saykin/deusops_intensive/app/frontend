FROM node:18-alpine as builder
WORKDIR /app
ARG DEV_DOMAIN
COPY ["package.json", "package-lock.json", "./"]
RUN ["npm", "install"]
COPY ["public/", "./public/"]
COPY ["src/", "./src/"]
RUN sed -ie "s/http:\/\/localhost:8080/https:\/\/${DEV_DOMAIN}/g" ./src/components/Student.js
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN ["npm", "run", "build", "--prod"]
RUN ["/bin/sh", "-c", "find . ! -name build ! -name node_modules -maxdepth 1 -mindepth 1 -exec rm -rf {} \\;"]

FROM node:18-alpine
WORKDIR /app
# COPY --from=builder /app/src/components/Student.js ./
COPY --from=builder /app/ ./
EXPOSE 3000
RUN npm install -g serve
ENTRYPOINT serve -s build
# CMD /bin/sh
